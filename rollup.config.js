import resolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';

export default {
    input: ['src/european-diploma-supplement.js'],
    output: [
        {
            file: 'dist/eds-render.iife.js',
            format: 'iife',
            sourcemap: true
        },
        {
            file: 'dist/eds-render.es.js',
            format: 'es',
            sourcemap: true
        },
        {
            file: 'dist/eds-render.umd.js',
            format: 'umd',
            sourcemap: true
        }
    ],
    plugins: [
        resolve(),
        babel()
    ]
};
