function getFile(file) {
    return new Promise((resolve, reject) => {
        let request = new XMLHttpRequest();
        request.onload = (event) => { resolve(request.response, event) };
        request.onerror = reject;
        request.open("GET", file);
        request.send();
    });
}

function getJSON(file) {
    return new Promise((resolve, reject) => {
        getFile(file).then(
            (data) => resolve(JSON.parse(data))
        ).catch(reject)
    });
}

function insertEuropeanDiplomaSupplement(options) {
    const certificateFile = "certificate.json";
    getJSON(certificateFile).then((certificate) => {
        let component = document.createElement('european-diploma-supplement');

        // Apply options
        if (options && options instanceof Object) {
            let keys = Object.keys(options);
            keys.forEach((key) => {
                component[key] = options[key];
            });
        }

        component.certificate = certificate;
        document.body.append(component);
    }).catch((error) => {
        alert(`Error while loading certificate: ${error}`);
    });
}
