export default {
  "ca": {
    "messages": {
      "print": "Imprimir",
      "printTitle": "Suplement Europeu al Diploma"
    },
    "recipient": {
      "title": "Dades de la persona titulada",
      "familyName": "Cognoms",
      "givenName": "Nom",
      "birthdate": "Data de naixement (dd/mm/aaaa)",
    },
    "formalization": {
      "registryCode": "Número d'identificació de l'estudiant"
    },
    "badge": {
      "title": "Informació sobre la titulació",
      "name": "Denominació de la titulació i del diploma conferit",
      "mainField": "Principals camps d'estudi de la titulació i orientació",
      "awardingInstitution": "Nom i naturalea de la institució que ha conferit el diploma",
      "administeringInstitution": "Nom i naturalesa de la institució en què es van impartir els estudis",
      "language": "Llengua(gües) utilitzada(es) en la docència i els exàmens",
    },
    "badgeLevel": {
      "title": "Informació sobre el nivell de la titulació",
      "studiesLevel": "Nivell de la titulació",
      "studiesLength": "Durada oficial del programa",
      "access": "Requisits d'accés"
    },
    "studies": {
      "title": "Informació sobre el contingut i els resultats obtinguts",
      "mode": "Forma d'estudi",
      "requirements": "Requisits del programa",
      "grades": "Descripció dels continguts (1)",
      "subject": "Assignatura",
      "gradingScheme": "Sistema de qualificació",
      "qualification": "Qualificació global de la persona titulada (2)",
      "qualificationPrefix": "Qualificació global: ",
      "qualificationSuffix": " punts"
    },
    "assertion": {
      "title": "Informació de la funció de la titulació",
      "further": "Accés a estudis ulteriors",
      "competences": "Objectius formatius i perfil de competències"
    },
    "extraInfo": {
      "title": "Informació addicional"
    },
    "signature": {
      "title": "Certificació del suplement",
      "signature": "Signatura",
      "date": "Data (dd/mm/aaaa)",
      "stamp": "Segell oficial",
      "rector": "El rector",
      "manager": "La gerent",
      "stampMessage": "En les versions digitals el segell no és necessari"
    },
    "educationSystem": {
      "title": "Informació sobre el sistema nacional d'educació superior"
    },
    "footnotes": [
        "(1) T: Tipus d'assignatura (OB: obligatòria, OP: optativa, LL: lliure elecció), S:" +
        "semestre o curs, A: any, Q: qualificació (CO: convalidada), C: crèdits europeus, QE: qualificació europea " +
        "(A: 10% millors notes, B: 25% millors notes següents, C: 30% millors notes següents, D: 25% millors notes " +
        "següents, E: 10% millors notes següents), M: Mobilitat acadèmica internacional.",
        "(2) La qualificació global es calcula mitjançant el criteri següent: sumatori dels crèdits" +
        "superats per l'alumne multiplicats cada un pel valor de la qualificació que" +
        "correspongui, a partir de la taula d'equivalències que s'especifica a continuació, i" +
        "dividit pel nombre de crèdits superats per l'alumne. Aprovat: 1 punt, Notable: 2" +
        "punts, Excel·lent: 3 punts, Matrícula d'honor: 4 punts."
    ],
    "sidebar": {
      "title": "Suplement al diploma",
      "description": "L'objectiu del Suplement al diploma és oferir informació independent i suficient per afavorir la transparència internacional i el reconeixement just, acadèmic i professional de les titulacions (diplomes, títols, certificats, etc.).\n\n" +
          "Està dissenyat per descriure la naturalesa, el nivell, el context, el contingut i l'estatus dels estudis realitzats satisfactòriament per l'interessat/ada, i amb els quals s'obté el títol dle qual es desprèn aquest Suplement. El Suplement al diploma compta amb vuit seccions. En cas que alguna d'elles no presenti informació s'especificarà el motiu."
    }
  },
  "en": {
    "messages": {
      "print": "Print",
      "printTitle": "European Diploma Supplement"
    },
    "recipient": {
      "title": "Information identifying the holder of the qualification",
      "familyName": "Family name(s)",
      "givenName": "Given name(s)",
      "birthdate": "Date of birth (dd/mm/yyyy)"
    },
    "formalization": {
      "registryCode": "Student identification number or code"
    },
    "badge": {
      "title": "Information identifying the qualification",
      "name": "Name of the qualification and title conferred (in original language)",
      "mainField": "Main field(s) of study for the qualification",
      "awardingInstitution": "Name (in original language) and status of awarding institution",
      "administeringInstitution": "Name (in original language) and status of institution administering studies",
      "language": "Language(s) of instruction / examination"
    },
    "badgeLevel": {
      "title": "Information on the level of the qualification",
      "studiesLevel": "Level of qualification",
      "studiesLength": "Official length of programme",
      "access": "Access requirements"
    },
    "studies": {
      "title": "Information on the contents and results gained",
      "mode": "Mode of study",
      "requirements": "Programme requirements",
      "grades": "Programme details (1)",
      "subject": "Subject",
      "gradingScheme": "Grading scheme",
      "qualification": "Overall classification of the qualification",
      "qualificationPrefix": "Overall classification: ",
      "qualificationSuffix": " points"
    },
    "assertion": {
      "title": "Information on the function of the qualification",
      "further": "Access to further study",
      "competences": "Professional status and competences",
    },
    "extraInfo": {
      "title": "Additional information"
    },
    "signature": {
      "title": "Certification of the Supplement",
      "signature": "Signature",
      "date": "Date (dd/mm/yyyy)",
      "stamp": "Official stamp or seal",
      "rector": "Principal",
      "manager": "Manager",
      "stampMessage": "In digital versions of certificates, the stamp is not" +
          " needed"
    },
    "educationSystem": {
      "title": "Information on the national higher education system"
    },
    "footnotes": [
      "(1) T: Type of Subject (OB: Obligatory; OP: Optional; LL: Free Choice), S: Semester or" +
      "Course, A: Year, Q: Grade (CO: Validated), C: European Credits, QE: European grading system (A: top 10%, B: " +
      "Next 25%, C: Next 30%, D: Next 25%, E: Next 10%), M: International academic mobility",
        "(2) The overall classification is calculated by means of the following criteria: addition of" +
        "the credits passed by the student multiplied each one by the value of the" +
        "corresponding mark from the table of equivalences that follows, and divided by the" +
        "number of credits passed by the student. Pass: 1 point, Very good: 2 points," +
        "Excellent: 3 points, Excellent with honours: 4 points."
    ],
    "sidebar": {
      "title": "Diploma Supplement",
      "description": "The purpose of the Diploma Supplement is to provide sufficient indepent data to improve the international transperncy and fair academic and professional reconognition of qualifications (diplomas, degrees, certificates, etc.).\n\n" +
          "It is designed to provide a description of the nature, level, context, content and status of the studies that were pursued and sucessfully completed by the individual named on the original qualification to which this supplement is appended. Information in all eight sections should be provided. Where information is not provided, an explanation should give the reason why."
    }
  }
}
