import {css} from 'lit-element'
export default css`@page{size:A4 portrait;margin:1cm 0.75cm}body{padding:0;margin:0.75cm;font-size:3mm;font-family:'Roboto', sans-serif}html{padding:0;margin:0}paper-button#print{display:none}
`;
