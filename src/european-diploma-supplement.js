import {html, LitElement} from "lit-element";
import {render} from "lit-html";
import i18n from "./i18n.js";
import moment from 'moment/src/moment';
import 'moment/src/locale/ca';
import educationSystemImage from "./educationSystemImage";
import CSS from "./european-diploma-supplement-css";
import '@polymer/font-roboto/roboto.js';
import printCSS from "./eds-print-css";

class EuropeanDiplomaSupplement extends LitElement {

    constructor(languagesToDisplay = ['ca', 'en']) {
        super();
        this.languagesToDisplay = EuropeanDiplomaSupplement.languagesAvailable;
        this.updateComplete.then(() => {
            this.afterRender()
        });
    }

    getEndorsements() {
        if (this.certificate.signature instanceof Object &&
            this.certificate.signature.endorsements instanceof Array)
            return this.certificate.signature.endorsements;
        else
            return null;
    }

    getEndorsementByClaimType(claimType) {
        let endorsements = this.getEndorsements();
        if (!endorsements) return null;

        let foundEndorsement = null;

        endorsements.forEach((endorsement) => {
            if (!endorsement.claim instanceof Object)
                return;

            let claim = endorsement.claim;

            if (claim.type instanceof Array) {
                claim.type.forEach((type) => {
                    if (type.includes(claimType)) {
                        foundEndorsement = endorsement;
                    }
                });
            }
        });

        return foundEndorsement;
    }

    getRecipient() {
        return this.getEndorsementByClaimType('RecipientClaim')
    }

    getEDS() {
        return this.getEndorsementByClaimType('EDSClaim')
    }

    getEDSSignatureByJobTitle(jobTitle) {
        let eds = this.getEDS();
        let signatureLines = eds.signatureLines;
        let foundSignature = null;
        if (signatureLines && signatureLines instanceof Array) {
            signatureLines.forEach((signature) => {
                if (signature.jobTitle && signature.jobTitle === jobTitle)
                    foundSignature = signature
            });
        }
        return foundSignature;
    }

    getEDSManagerSignature() {
        return this.getEDSSignatureByJobTitle('MANAGER');
    }

    getEDSRectorSignature() {
        return this.getEDSSignatureByJobTitle('RECTOR');
    }

    getFormalization() {
        return this.certificate.officialValidation || null;
    }

    translate(lang, section, item = EuropeanDiplomaSupplement.defaultItem) {

        const translationQuery = `["${lang}"]["${section}"]["${item}"]`;

        // Not enough arguments
        if (!lang || !section || !item)
            return `TranslateCallError(${translationQuery})`;

        // Language not present
        let languageTranslations = EuropeanDiplomaSupplement.translations[lang];
        if (!languageTranslations)
            return translationQuery;

        // Section not present
        let sectionTranslations = languageTranslations[section];
        if (!sectionTranslations)
            return translationQuery;

        // Item not present
        let translation = sectionTranslations[item];
        if (!translation)
            return translationQuery;

        return translation;
    }

    renderTitleRowSpan(lang, section, item = EuropeanDiplomaSupplement.defaultItem) {
        return html`<span class='${lang}'>${this.translate(lang, section, item)}</span>`
    }

    renderTitleRowSpans(section, item = EuropeanDiplomaSupplement.defaultItem, newLine = false) {
        let template = html``;
        this.languagesToDisplay.forEach((language, index) => {
            let separator = this.languagesToDisplay.length - 1 !== index ? (newLine ? html`<br>` : EuropeanDiplomaSupplement.defaultSeparator) : '';
            template = html`${template}${this.renderTitleRowSpan(language, section, item)}${separator}`
        });
        return template;
    }

    renderTitleRow(number, section, item = 'title', head = false, newLine = false) {
        let title = this.renderTitleRowSpans(section, item, newLine);
        let headClass = head ? 'head' : '';

        return html`<div class="row title ${headClass}">
                        <div class="number">${number}.</div>
                        <div class="title">${title}</div>
                    </div>`
    }

    renderDataRow(data, dataClass = '', date = false) {
        return html`<div class="row data simple">
                        <div class="number"></div>
                        <div class="data ${dataClass}">${date ? moment(data).format('DD/MM/YYYY') : data}</div> 
                    </div>`
    }

    renderMultilanguageDataRow(container, item) {
        let dataCells = this.renderDataCells(container, item);
        return html`<div class="row data multilanguage">
                        <div class="number"></div>
                        ${dataCells}
                    </div>`
    }

    renderMultilanguageDataRows(container, item) {

        // TODO: What if different sizes, not available, ...
        let rows = html``;
        container[this.languagesToDisplay[0]][item].forEach((e, subitem) => {

            let cells = html``;
            this.languagesToDisplay.forEach((language) => {
                cells = html`${cells}<div class="data ${language}">${container[language][item][subitem]}</div>`
            });

            rows = html`${rows}<div class="row data multilanguage">
                        <div class="number"></div>
                        ${cells}
                    </div>`
        });

        return rows;

    }

    getContainerTransatedData(container, language, item) {
        let defaultTranslation = `["${language}"]["${item}"]`;
        let translatedContainer = container[language];
        return translatedContainer ?
            (translatedContainer[item] || defaultTranslation) :
            defaultTranslation;
    }

    renderDataCells(container, item) {
        let cells = html``;
        this.languagesToDisplay.forEach((language) => {
            cells = html`${cells}<div class="data ${language}">${this.getContainerTransatedData(container, language, item)}</div>`
        });
        return cells;
    }

    renderGradesTable(container) {

        let grades = 'grades';

        let headerCells = html``;
        this.languagesToDisplay.forEach((language) => {
            headerCells = html`${headerCells}<div class="subject ${language}">${this.translate(language, 'studies', 'subject')}</div>`
        });
        headerCells = html`
            ${headerCells}
            <div class="item">T</div>
            <div class="item">S</div>
            <div class="item">A</div>
            <div class="item">M</div>
            <div class="item">Q</div>
            <div class="item">C</div>
            <div class="item">QE</div>
        `;
        let headerRow = html`<div class="row grades header">${headerCells}</div>`;

        // TODO: What if different grade num?
        let contentRows = html``;
        container[this.languagesToDisplay[0]][grades].forEach((grade, gradeIndex) => {
            let subjectItems = html``;
            this.languagesToDisplay.forEach((language) => {
                let subjectName;
                if (container[language] instanceof Object &&
                    container[language][grades] instanceof Array &&
                    container[language][grades].length > 0 && gradeIndex < container[language][grades].length &&
                    container[language][grades][gradeIndex] instanceof Object &&
                    container[language][grades][gradeIndex]['name']
                ) {
                    subjectName = container[language][grades][gradeIndex]['name'];
                } else {
                    subjectName = `["${language}"]["grades"][${gradeIndex}]["name"]`;
                }
                subjectItems = html`${subjectItems}
                                  <div class="subject ${language}">
                                    ${subjectName}
                                  </div>
                            `;
            });

            let contentRow = html`${subjectItems}
                              <div class="item">${grade.choice}</div>
                              <div class="item">${grade.semester}</div>
                              <div class="item">${grade.year}</div>
                              <div class="item">${grade.mobility}</div>
                              <div class="item">${grade.grade}</div>
                              <div class="item">${grade.credits}</div>
                              <div class="item">${grade.europeanQualification || '-'}</div>
            `;
            contentRows = html`${contentRows}<div class="row grades content">${contentRow}</div>`
        });

        return html`
            ${headerRow}
            ${contentRows}
        `
    }

    renderMultilanguageQualification(container) {
        let cells = html``;
        this.languagesToDisplay.forEach((language) => {
            cells = html`${cells}<div class="data ${language}">
                ${this.translate(language, 'studies', 'qualificationPrefix')}
                ${this.getContainerTransatedData(container, language, 'qualification')}
                ${this.translate(language, 'studies', 'qualificationSuffix')}
            </div>`
        });
        return html`<div class="row data multilanguage">
                        <div class="number"></div>
                        ${cells}
                    </div>`
    }

    renderSignature(endorsement) {
        let headerRow = html`<div class="row signature header">
                                <div class="signature">${this.renderTitleRowSpans('signature', 'signature')}</div>
                                <div class="signature"></div>
                                <div class="date">${this.renderTitleRowSpans('signature', 'date')}</div>
                                <div class="stamp">${this.renderTitleRowSpans('signature', 'stamp')}</div>
                             </div>`;

        let rectorSignature = this.getEDSRectorSignature();
        let managerSignature = this.getEDSManagerSignature();
        let contentRow = html`<div class="row signature">
                                <div class="signature">
                                    ${rectorSignature ? html`<img src="${rectorSignature.image}">` : html``}
                                </div>
                                <div class="signature">
                                    ${managerSignature ? html`<img src="${managerSignature.image}">` : html``}
                                </div>
                                <div class="date">${moment(endorsement.issuedOn).format('LL')}</div>
                                <div class="stamp">
                                <i>${this.translate(this.languagesToDisplay[0], 'signature', 'stampMessage')}</i><br>
                                <i class="${this.languagesToDisplay[1]}">${this.translate(this.languagesToDisplay[1], 'signature', 'stampMessage')}</i>
                                </div>
                              </div>`;

        let footerRow = html`<div class="row signature">
                                <div class="signature">
                                    ${this.translate(this.languagesToDisplay[0], 'signature', 'rector')}
                                    <br>${rectorSignature.name}
                                </div>
                                <div class="signature">
                                    ${this.translate(this.languagesToDisplay[0], 'signature', 'manager')}
                                    <br>${managerSignature.name}
                                </div>
                                <div class="date"></div>
                                <div class="stamp"></div>
                              </div>`;

        return html`${headerRow}${contentRow}${footerRow}`;
    }

    renderFootnotes() {

        // TODO: What if different sizes, not available, ...
        let rows = html``;
        EuropeanDiplomaSupplement.translations[this.languagesToDisplay[0]]['footnotes'].forEach((e, subitem) => {

            let cells = html``;
            this.languagesToDisplay.forEach((language) => {
                cells = html`${cells}<div class="data ${language}">${EuropeanDiplomaSupplement.translations[language]['footnotes'][subitem]}</div>`
            });

            rows = html`${rows}<div class="row data multilanguage footnotes">
                        ${cells}
                    </div>`
        });

        return rows;

    }

    render() {
        if (this.certificate) {

            let recipientEndorsement = this.getRecipient();
            let recipientClaim;
            if (recipientEndorsement)
                recipientClaim = recipientEndorsement.claim;

            let edsEndorsement = this.getEDS();
            let edsClaim;
            if (edsEndorsement)
                edsClaim = edsEndorsement.claim;

            let formalizationEndorsement = this.getFormalization();
            let formalizationClaim;
            if (formalizationEndorsement)
                formalizationClaim = formalizationEndorsement.claim;

            if (!edsEndorsement)
                return html``;

            let sidebarHTML = html`
                <div id="eds-info">
                    <div id="university-logo" class="aside">
                        <img src="${this.certificate.badge.issuer.image}">
                    </div>
                    <div class="eds-info-title">
                        <p class="ca">
                            ${this.translate('ca', 'sidebar', 'title')}
                        </p>
                        <p class="en">
                            ${this.translate('en', 'sidebar', 'title')}
                        </p>
                    </div>
                    <div class="row eds-info-description">
                        <p class="ca">
                            ${this.translate('ca', 'sidebar', 'description')}
                        </p>
                        <p class="en">
                            ${this.translate('en', 'sidebar', 'description')}
                        </p>                    
                    </div>
                </div>
            `;

            let recipientHTML = recipientEndorsement ? html`
                ${this.renderTitleRow('1', 'recipient', 'title', true, true)}
                ${this.renderTitleRow('1.1', 'recipient', 'familyName', false, false)}
                ${this.renderDataRow(recipientClaim.familyName)}
                ${this.renderTitleRow('1.2', 'recipient', 'givenName', false, false)}
                ${this.renderDataRow(recipientClaim.givenName)}
                ${this.renderTitleRow('1.3', 'recipient', 'birthdate', false, false)}
                ${this.renderDataRow(recipientClaim.birthdate, '', true)}
            ` : html``;

            let formalizationHTML = formalizationEndorsement ? html`
                ${this.renderTitleRow('1.4', 'formalization', 'registryCode', false, false)}
                ${this.renderDataRow(formalizationClaim.registryCode)}
            ` : html``;

            let badgeHTML = html`
                ${this.renderTitleRow('2', 'badge', 'title', true, false)}
                ${this.renderTitleRow('2.1', 'badge', 'name', false, true)}
                ${this.renderDataRow(this.certificate.badge.name, 'badge-name')}
                ${this.renderTitleRow('2.2', 'badge', 'mainField', false, true)}
                ${this.renderMultilanguageDataRow(edsClaim, 'mainField')}
                ${this.renderTitleRow('2.3', 'badge', 'awardingInstitution', false, true)}
                ${this.renderMultilanguageDataRow(edsClaim, 'awardingInstitution')}
                ${this.renderTitleRow('2.4', 'badge', 'administeringInstitution', false, true)}
                ${this.renderMultilanguageDataRow(edsClaim, 'administeringInstitution')}
                ${this.renderTitleRow('2.5', 'badge', 'language', false, true)}
                ${this.renderMultilanguageDataRow(edsClaim, 'language')}
            `;

            let badgeLevelHTML = html`
                ${this.renderTitleRow('3', 'badgeLevel', 'title', true, true)}
                ${this.renderTitleRow('3.1', 'badgeLevel', 'studiesLevel', false, false)}
                ${this.renderMultilanguageDataRow(edsClaim, 'studiesLevel')}
                ${this.renderTitleRow('3.2', 'badgeLevel', 'studiesLength', false, false)}
                ${this.renderMultilanguageDataRow(edsClaim, 'studiesLength')}
                ${this.renderTitleRow('3.3', 'badgeLevel', 'access', false, false)}
                ${this.renderMultilanguageDataRow(edsClaim, 'access')}
            `;

            let studiesHTML = html`
                ${this.renderTitleRow('4', 'studies', 'title', true, true)}
                ${this.renderTitleRow('4.1', 'studies', 'mode', false, false)}
                ${this.renderMultilanguageDataRow(edsClaim, 'mode')}
                ${this.renderTitleRow('4.2', 'studies', 'requirements', false, false)}
                ${this.renderMultilanguageDataRow(edsClaim, 'requirements')}
                ${this.renderTitleRow('4.3', 'studies', 'grades', false, false)}
                ${this.renderGradesTable(edsClaim)}
                ${this.renderTitleRow('4.4', 'studies', 'gradingScheme', false, false)}
                ${this.renderMultilanguageDataRow(edsClaim, 'gradingScheme')}
                ${this.renderTitleRow('4.5', 'studies', 'qualification', false, false)}
                ${this.renderMultilanguageQualification(edsClaim)}
            `;

            let assertionHTML = html`
                ${this.renderTitleRow('5', 'assertion', 'title', true, false)}
                ${this.renderTitleRow('5.1', 'assertion', 'further', false, false)}
                ${this.renderMultilanguageDataRow(edsClaim, 'further')}
                ${this.renderTitleRow('5.2', 'assertion', 'competences', false, false)}
                ${this.renderMultilanguageDataRows(edsClaim, 'competences')}
            `;

            let extraInfoHTML = html`
                ${this.renderTitleRow('6', 'extraInfo', 'title', true, false)}
                ${this.renderMultilanguageDataRows(edsClaim, 'extraInfo')}
            `;

            let signatureHTML = html`
                ${this.renderTitleRow('7', 'signature', 'title', true, false)}
                ${this.renderSignature(edsEndorsement)}
            `;

            let educationSystemHTML = html`
                ${this.renderTitleRow('8', 'educationSystem', 'title', true, true)}
                <div class="row education-system">
                    <img src="${edsClaim[this.languagesToDisplay[0]]["educationSystem"]}" alt="educationSystem">
                </div>
            `;

            let printButtonHTML = this.noprint ? html`` : html`
                <paper-button raised id="print" @click="${this.print}">
                    <iron-icon icon="print"></iron-icon>
                    ${this.translate(this.languagesToDisplay[0], 'messages', 'print')}
                </paper-button>
            `;

            let footnotesHTML = this.renderFootnotes();

            return html`
                ${sidebarHTML}
                ${recipientHTML}${formalizationHTML}
                ${badgeHTML}
                ${badgeLevelHTML}
                ${studiesHTML}
                ${assertionHTML}
                ${extraInfoHTML}
                ${signatureHTML}
                ${educationSystemHTML}
                ${footnotesHTML}
                ${printButtonHTML}
            `
        } else {
            console.warn('No certificate provided');
            return html``
        }
    }

    afterRender() {
        if (this.autoprint) {
            this.print();
        }
    }

    print() {
        let printWindow = window.open(
            "",
            "", "");
        render(
            this.render(),
            printWindow.document.body
        );
        printWindow.document.title = this.translate(this.languagesToDisplay[0], 'messages', 'printTitle');
        // This CSS
        printWindow.document.head.appendChild(EuropeanDiplomaSupplement.styleElement(
            printWindow.document
        ));
        // Print CSS
        printWindow.document.head.appendChild(EuropeanDiplomaSupplement.styleElement(
            printWindow.document,
            printCSS.cssText
        ));
        // TODO: This should be done better
        // Fonts
        let links = document.head.getElementsByTagName('link');
        for(let link of links) {
            if(link.href.includes('Roboto')) {
                printWindow.document.head.appendChild(link);
            }
        }
        EuropeanDiplomaSupplement.showPrintDialog(printWindow, true);
    }

    static showPrintDialog(printWindow, closeAfter = true) {
        if (closeAfter) {
            printWindow.addEventListener("afterprint", () => {
                printWindow.close();
            });
        }
        printWindow.print();
    }

    static styleElement(document, styles = EuropeanDiplomaSupplement.styles.cssText) {
        let styleElement = document.createElement('style');
        styleElement.innerHTML = styles;
        return styleElement;
    }

    static get translations() {
        return i18n;
    }

    static get languagesAvailable() {
        return Object.keys(this.translations)
    }

    static get defaultItem() {
        return 'title';
    }

    static get defaultSeparator() {
        return html`&nbsp;/&nbsp;`;
    }

    static get styles() {
        return CSS;
    }

    static get properties() {
        return {
            certificate: {
                type: Object,
                value: {},
            },
            noprint: {
                type: Boolean,
                value: false
            },
            autoprint: {
                type: Boolean,
                value: false
            }
        };
    }
}

customElements.define('european-diploma-supplement', EuropeanDiplomaSupplement);
