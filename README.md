# European Diploma Supplement render
This [web component][web-component] renders an [European Diploma 
Supplement (EDS)][eds] endorsement as specified per the [Cowcerts standards for 
Education][cowcerts-edu] inside a [Cowcerts][cowcerts] academic certificate.

It uses the Andorran [European Diploma Supplement][eds] format as the view.

## Usage

Simply include the `european-diploma-supplement` element in your `HTML` and
use the `certificate` attribute to specify the certificate where the EDS is 
inside.

```html
<european-diploma-supplement certificate="{{myCertificate}}"></european-diploma-supplement>
```

> You have to include the web components library and the JavaScript module that
> defines the custom element.

> Also you will have to include some scripts to display the print button
> Look at the [demo](demo) folder for a more complete example

### Options
You can specify several options, not just the mandatory `certificate` one.

#### Hide print button
You can hide the print button setting the `noprint` attribute to `true`

```html
<european-diploma-supplement noprint certificate="{{myCertificate}}"></european-diploma-supplement>
```

#### Autoprint

Using the `autoprint` attribute, the print dialog will automatically appear

```html
<european-diploma-supplement autoprint certificate="{{myCertificate}}"></european-diploma-supplement>
```

## Development
### Serving the certificate
To serve the certificate, use the `start` npm script:

```shell
npm run start
```

A local server will be available on http://localhost:8081 with the web 
component rendered with a [sample certificate][sample-certificate]

### Automatically refreshing browser on changes
If you are serving the certificate via the `npm run start` command, you can 
also reload automatically the browser if any of the WebComponent sources or demo
files change.

```shell
npm run watch
```

### Build
With [rollup.js][rollup] you can bundle everything in a module, just use the
`build` npm script:

```shell
npm run build
```

> The file [demo/bundled.html](demo/bundled.html) contains a 
> demo for the rendered version (the one compiled by [rollup.js][rollup])

[web-component]: https://www.webcomponents.org/
[eds]: https://europass.cedefop.europa.eu/documents/european-skills-passport/diploma-supplement
[cowcerts]: https://cowcerts.org 
[cowcerts-edu]: https://cowcerts.rtfd.io/en/latest/edu
[sample-certificate]: ./demo/certificate.json
[rollup]: https://rollupjs.org
[livereload]: http://livereload.com/

**\<\> with ♥ by [BTC Assessors](https://btcassessors.com): 
[@davidlj95](https://gitlab.com/davidlj95), [@ccebrecos](https://gitlab.com/ccebrecos)**

[![BTC Assessors](https://i.imgur.com/7nzUvR0.png)](https://www.btcassessors.com)
